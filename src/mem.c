#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  query = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  void * new_addr = map_pages(addr, query, MAP_FIXED_NOREPLACE);
  if(new_addr == MAP_FAILED)
    new_addr = map_pages(addr, query, 0);
  if(new_addr == MAP_FAILED)
    return REGION_INVALID;
  block_init(new_addr, (block_size){query}, NULL);
  return (struct region) {.addr = new_addr, .size = query, .extends = new_addr == addr};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if(!(block && block->next && mergeable(block, block->next)))
    return false;
  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;
  return true;
}



/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header * cur = HEAP_START;
  struct block_header * old;
  while (cur)
  {
    cur->is_free = true;
    while(cur->next && blocks_continuous(cur, cur->next)) {
      cur->next->is_free = true;
      try_merge_with_next(cur);
    }
    old = cur;
    cur = cur->next;
    munmap(old, size_from_capacity(old->capacity).bytes);
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  if(!block || !block_splittable(block, query)){
    return false;
  }
  block_size block1_size = size_from_capacity((block_capacity){query});
  block_size block2_size = (block_size){size_from_capacity(block->capacity).bytes - block1_size.bytes};
  block_init(block->contents + query, block2_size, block->next);
  block_init(block, block1_size, block->contents + query);
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if(!block)
    return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
  struct block_header *cur = block;
  while(cur) {
    while (try_merge_with_next(cur));
    if(cur->is_free && block_is_big_enough(sz, cur))
      return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = cur};
    block = cur;
    cur = cur->next;
  }
  return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct block_search_result res = find_good_or_last(block, query);
  if(res.type != BSR_FOUND_GOOD_BLOCK)
    return res;
  split_if_too_big(res.block, query);
  res.block->is_free = false;
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  if(!last)
    return NULL;
  struct region region = alloc_region(block_after(last), query);
  if(region_is_invalid(&region))
    return NULL;
  if(region.extends) {
    last->next = region.addr;
    return try_merge_with_next(last) ? last : (struct block_header*) region.addr;
  } else {
    block_init(region.addr, (block_size){region.size}, NULL);
    last->next = region.addr;
    return (struct block_header*) region.addr;
  }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  query = max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result res = try_memalloc_existing(query, heap_start);
  switch (res.type)
  {
  case BSR_FOUND_GOOD_BLOCK:
    return res.block;
  case BSR_CORRUPTED:
    return NULL;
  case BSR_REACHED_END_NOT_FOUND:
    heap_start = grow_heap(res.block, query);
    return heap_start ? try_memalloc_existing(query, heap_start).block : heap_start;
  }
  return NULL;
}

void* _malloc( size_t query ) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
